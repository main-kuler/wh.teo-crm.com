<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Users', 'icon' => 'fa  fa-users', 'url' => ['/user'],],
                    ['label' => 'Candidates', 'icon' => 'fa  fa-user-o', 'url' => ['/candidate'],],
                    ['label' => 'Requests', 'icon' => 'fa  fa-file', 'url' => ['/request'],],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
