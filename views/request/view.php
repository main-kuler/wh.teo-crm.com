<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
?>
<div class="request-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'client_id',
            'status',
            'zip',
            'state',
            'bank',
            'sum',
            'status_changed_at',
            'created_at',
        ],
    ]) ?>

</div>
