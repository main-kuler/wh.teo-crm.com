<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Recorders */
?>
<div class="recorders-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text:ntext',
            'url:url',
        ],
    ]) ?>

</div>
