<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Candidate */
?>
<div class="candidate-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'state',
            'bank',
            'account',
            'checking_number',
            'routing_number',
            'address',
        ],
    ]) ?>

    <div class="row">
        <div class="col-md-12">
            <h4 style="margin-top: 10px;">Файлы</h4>
            <?= $this->render('@app/views/candidate-file/index', [
                'searchModel' => $filesSearchModel,
                'dataProvider' => $filesDataProvider,
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= \kato\DropZone::widget([
                'id'        => 'dzImage', // <-- уникальные id
                'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file', 'candidate_id' => $model->id ]),
                'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                'options' => [
                    'maxFilesize' => '2',
                ],
                'clientEvents' => [
                    'complete' => "function(file){ $.pjax.reload('#file-pjax'); }",
                ],
            ]);?>
        </div>
    </div>
</div>

</div>
