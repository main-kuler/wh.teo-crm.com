<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property int $client_id Клиент
 * @property int $status
 * @property string $zip
 * @property string $state
 * @property string $bank
 * @property double $sum
 * @property string $status_changed_at
 * @property string $created_at
 *
 * @property User $client
 */
class Request extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_DOCS_LOADED = 1;
    const STATUS_DONE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => null,
                'createdByAttribute' => 'client_id',
                'value' => Yii::$app->user->getId(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'status'], 'integer'],
            [['sum'], 'number'],
            [['status_changed_at', 'created_at'], 'safe'],
            [['zip', 'state', 'bank'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'status' => 'Status',
            'zip' => 'Zip',
            'state' => 'State',
            'bank' => 'Bank',
            'sum' => 'Sum',
            'status_changed_at' => 'Status Changed At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_NEW => 'New',
            self::STATUS_DOCS_LOADED => 'Documents loaded',
            self::STATUS_DONE => 'Done'
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if(in_array('status', array_keys($changedAttributes))){
            if($changedAttributes['status'] != $this->status) {
                $this->status_changed_at = date('Y-m-d H:i:s');
                $this->save(false);
            }
        }
    }

    /**
     * @return mixed|null
     */
    public function getStateName()
    {
        $model = State::find()->where(['alias' => $this->state])->one();

        if($model){
            return $model->name;
        }

        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(User::className(), ['id' => 'client_id']);
    }
}
