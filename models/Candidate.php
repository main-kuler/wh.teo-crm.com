<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "candidate".
 *
 * @property int $id
 * @property string $state Штат
 * @property string $bank Банк
 * @property string $account Счет
 * @property string $checking_number
 * @property string $routing_number
 * @property string $address
 *
 * @property string $stateName
 */
class Candidate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state', 'bank', 'account', 'checking_number'], 'required'],
            [['state', 'bank', 'account', 'checking_number', 'routing_number', 'address'], 'string', 'max' => 255],
            [['checked'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state' => 'State',
            'bank' => 'Bank',
            'account' => 'Account',
            'checking_number' => 'Checking Number',
            'routing_number' => 'Routing Number',
            'address' => 'Address',
            'checked' => 'Checked'
        ];
    }

    /**
     * @return mixed|null
     */
    public function getStateName()
    {
        $model = State::find()->where(['alias' => $this->state])->one();

        if($model){
            return $model->name;
        }

        return null;
    }
}
