<?php

namespace app\validators;

use Yii;
use yii\validators\Validator;
use app\models\Clients;

/**
 * Class UniquePhoneValidator
 * @package app\validators
 *
 * Отвечает за валидацию уникальности номеров телефонов в таблице клиентов, значения которых
 * записыны в двух разных полях ( «phone_number» и «phone_number_second» ) в талице клиентов ( «clients» )
 */
class UniquePhoneValidator extends Validator
{
    /**
     * @inheritdoc
     * @param Clients $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if($model->id == null){
            $duplicate = Clients::findByPhoneTwice($model->$attribute);
        } else {
            $duplicate = Clients::findByPhoneTwice($model->$attribute, $model->id);
        }

        if($duplicate != null){
            $model->addError($attribute, 'Данный номер уже зарегистрирован');
        }
    }
}

?>