<?php

namespace app\controllers;

use yii\web\Controller;

/**
 * UserController implements the CRUD actions for User model.
 */
class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Render Yandex maps.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
