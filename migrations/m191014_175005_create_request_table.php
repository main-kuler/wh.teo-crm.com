<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request`.
 */
class m191014_175005_create_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('request', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->comment('Клиент'),
            'status' => $this->integer(),
            'zip' => $this->string(),
            'state' => $this->string(),
            'bank' => $this->string(),
            'sum' => $this->float(),
            'status_changed_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-request-client_id',
            'request',
            'client_id'
        );

        $this->addForeignKey(
            'fk-request-client_id',
            'request',
            'client_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-request-client_id',
            'request'
        );

        $this->dropIndex(
            'idx-request-client_id',
            'request'
        );

        $this->dropTable('request');
    }
}
