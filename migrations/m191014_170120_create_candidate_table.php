<?php

use yii\db\Migration;

/**
 * Handles the creation of table `candidate`.
 */
class m191014_170120_create_candidate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('candidate', [
            'id' => $this->primaryKey(),
            'state' => $this->string()->comment('Штат'),
            'bank' => $this->string()->comment('Банк'),
            'account' => $this->string()->comment('Счет'),
            'checked' => $this->boolean()->defaultValue(false),
            'checking_number' => $this->string(),
            'routing_number' => $this->string(),
            'address' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('candidate');
    }
}
